provider "aws" {
    region = "us-east-1"
}
variable "ins_type" { default = "t2.micro" }

variable "ins_ami" {
    type = "map"
    default = {
        "centos" = "ami-0470b964333b2805d"
        "rhel"  = "ami-03587fa4048e9eb92"
        "amazon" = "ami-09c61c4850b7465cb"
    }
}
resource "aws_instance" "ins01" {
  instance_type = "${var.ins_type}"
  ami = "${var.ins_ami.centos}"
  tags ={
      Name = "Production"
      Env = "prod"
  }
  key_name = "ani_NV"
}



