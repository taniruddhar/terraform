provider "aws" {
    region = "ap-southeast-1"
}
resource "aws_instance" "ins01" {
    key_name = "radha_singapore"
    ami = "ami-0b4dd9d65556cac22"
    instance_type = "t2.micro"
    tags = {

        Name = "Prod"
    }
}
