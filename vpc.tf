resource "aws_vpc" "Prod" {

cidr_block = "${var.CIDR["VPC_CIDR"]}"
tags = {
    Name = "Prod_vpc"
    Env = "${var.env}"
}    
  
}

resource "aws_subnet" "Private_prod" {
    tags = {
        Name = "Private_prod"
        Env = "${var.env}"
    }
      vpc_id = "${aws_vpc.Prod.id}"
      cidr_block = "${var.CIDR["Subnet_pri"]}"

  
}

resource "aws_subnet" "Public_prod" {
    tags = {
        Name = "Public_prod"
        Env = "${var.env}"
    }
    vpc_id = "${aws_vpc.Prod.id}"
    cidr_block = "${var.CIDR["Subnet_pub"]}"
    map_public_ip_on_launch = true
  
}

resource "aws_internet_gateway" "IGW_Prod" {
 tags = {
     Name = "IGW_prod"
     Env = "${var.env}"
 }
    vpc_id = "${aws_vpc.Prod.id}"
}

resource "aws_route_table" "main_rt" {
  vpc_id = "${aws_vpc.Prod.id}"
  tags = {
      Name = "Main_rt"
      Env = "${var.env}"
  }
}

resource "aws_route_table_association" "asso_rt1_pub" {
    subnet_id = "${aws_subnet.Public_prod.id}"
    route_table_id = "${aws_route_table.main_rt.id}"

}
resource "aws_route" "rt_main_route" {
    route_table_id = "${aws_route_table.main_rt.id}"
    destination_cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.IGW_Prod.id}"

  
}
resource "aws_eip" "eip_nat" {
    tags = {
        Name = "eip_prod"
        Env = "${var.env}"
    }
  
}

resource "aws_nat_gateway" "nat_prod" {
    allocation_id = "${aws_eip.eip_nat.id}"
    subnet_id = "${aws_subnet.Public_prod.id}"
    tags = {
        Name = "nat_prod"
        Env = "${var.env}"
    }
  
}


resource "aws_route_table" "rt2_prod" {
    tags = {
        Name = "rt2_prod"
        Env = "${var.env}"
    }
    vpc_id = "${aws_vpc.Prod.id}"
    route = {
        cidr_block = "0.0.0.0/0"
        nat_gateway_id = "${aws_nat_gateway.nat_prod.id}"
    }
}
resource "aws_route_table_association" "asso_rt2_pri" {
    subnet_id = "${aws_subnet.Private_prod.id}"
    route_table_id = "${aws_route_table.rt2_prod.id}"
  
}
/*
resource "aws_instance" "pri_inst" {
    ami = "ami-02ccb28830b645a41"
    instance_type = "t2.micro"
    subnet_id = "${aws_subnet.Private_prod.id}"
  
}
resource "aws_instance" "pub_inst" {
    ami = "ami-02ccb28830b645a41"
    instance_type = "t2.micro"
    subnet_id = "${aws_subnet.Public_prod.id}" 
  
} */

