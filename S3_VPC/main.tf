provider "aws" {
  region = "us-east-1"
}
resource "aws_s3_bucket" "ani-5678" {
  bucket = "andy-to-58997895"
  acl = "public-read"
  tags ={
      Name = "My Bucket"
      Environment = "Dev"
  }
}
