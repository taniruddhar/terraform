provider "aws" {
    region = "ap-southeast-1"
}

variable "type_ins" { default = "t2.micro" }
variable "key" {default = "radha_singapore"}

variable "image" {
    type = "map"
    default = {
        "centos" = "ami-0b4dd9d65556cac22"
        "amazon" = "ami-048a01c78f7bae4aa"
        "rhel"  = "ami-04a2d6660f1296314"  
    }
  
}

resource "aws_instance" "ins01" {
    tags = {
        Name = "prod"
    }
    ami = "${var.image.centos}"
    key_name = "${var.key}"
    instance_type = "${var.type_ins}"
}


