provider "aws" {
    region = "us-east-1"
}
variable "ins_type" { default = "t2.micro"}
variable "ins_key" { default = "ani_NV"}
variable "ins_ami" {
  type = "map"
  default ={
    "laptop" = "ami-0470b964333b2805d"
    "home" = "ami-06764be923753c5e1"
    "mobile" = "ami-073440407c04ed186"
}
}

resource "aws_launch_configuration" "home-lc" {
  name = "home-lc"
  image_id = "${var.ins_ami.home}"
  instance_type = "${var.ins_type}"

}

resource "aws_launch_configuration" "mobile-lc" {
    name = "mobile-lc"
    image_id = "${var.ins_ami.mobile}"
    instance_type = "${var.ins_type}"
    
}
resource "aws_launch_configuration" "laptop-lc" {
    name = "laptop-lc"
    image_id = "${var.ins_ami.laptop}"
    instance_type = "${var.ins_type}"
    
}
resource "aws_lb_target_group" "homet-tg" {
    name = "hometg"
    port = 80
    target_type = "instance"
    vpc_id = "vpc-9b5923e1"
    protocol = "HTTP"
}
resource "aws_lb_target_group" "mobile-tg" {
    name = "mobiletg"
    port = 80
    target_type = "instance"
    vpc_id = "vpc-9b5923e1"
    protocol = "HTTP"
}
resource "aws_lb_target_group" "laptop-tg" {
    name = "laptoptg"
    port = 80
    target_type = "instance"
    vpc_id = "vpc-9b5923e1"
    protocol = "HTTP"
}


resource "aws_autoscaling_group" "home-as" {
    name = "home-as"
    max_size = 4 
    min_size = 1
    desired_capacity = 2
    health_check_grace_period = 300
    health_check_type         = "ELB"
    launch_configuration = "${aws_launch_configuration.home-lc.id}"
    vpc_zone_identifier = ["subnet-e5738da8"]
    availability_zones = ["us-east-1a"]
    target_group_arns = ["${aws_lb_target_group.homet-tg.id}"]
}

resource "aws_autoscaling_group" "mobile-as" {
    name = "mobile-as"
    max_size = 4 
    min_size = 1
    desired_capacity = 2
    health_check_grace_period = 300
    health_check_type         = "ELB"
    launch_configuration = "${aws_launch_configuration.mobile-lc.id}"
    vpc_zone_identifier = ["subnet-e5738da8"]
    availability_zones = ["us-east-1a"]
    target_group_arns = ["${aws_lb_target_group.mobile-tg.id}"]
}

resource "aws_autoscaling_group" "laptop-as" {
    name = "laptop-as"
    max_size = 4 
    min_size = 1
    desired_capacity = 2
    health_check_grace_period = 300
    health_check_type         = "ELB"
    launch_configuration = "${aws_launch_configuration.laptop-lc.id}"
    vpc_zone_identifier = ["subnet-e5738da8"]
    availability_zones = ["us-east-1a"]
    target_group_arns = ["${aws_lb_target_group.laptop-tg.id}"]
}

resource "aws_alb" "myloadbal" {
    name = "appload"
    load_balancer_type ="application"
    subnets = ["subnet-aa6080a4","subnet-e5738da8"]
}

resource "aws_alb_listener" "alb_listener" {
    load_balancer_arn = "${aws_alb.myloadbal.id}"
    port = 80
    protocol = "HTTP"
    default_action {
        target_group_arn = "${aws_lb_target_group.homet-tg.id}"
        type = "forward"
    }
}

resource "aws_alb_listener_rule" "listener_rule" {
    listener_arn = "${aws_alb_listener.alb_listener.id}"
    priority = 99
    action {
        type = "forward"
        target_group_arn = "${aws_lb_target_group.homet-tg.id}"


    }
    condition {
        field = "path-pattern"
        values = ["/"]
    }
  
}

resource "aws_alb_listener_rule" "listener_rule1" {
    listener_arn = "${aws_alb_listener.alb_listener.id}"
    priority = 100
    action {
        type = "forward"
        target_group_arn = "${aws_lb_target_group.laptop-tg.id}"


    }
    condition {
        field = "path-pattern"
        values = ["/laptop/*"]
    }
  
}
resource "aws_alb_listener_rule" "listener_rule3" {
    listener_arn = "${aws_alb_listener.alb_listener.id}"
    priority = 110
    action {
        type = "forward"
        target_group_arn = "${aws_lb_target_group.mobile-tg.id}"


    }
    condition {
        field = "path-pattern"
        values = ["/mobile/*"]
    }
  
}







